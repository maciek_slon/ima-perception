#! /usr/bin/env python
# -*- coding: utf8 -*-

import rospy
from sensor_msgs.msg import PointCloud2, Image

import numpy as np
import ros_numpy as rnp
import cv2
from cv_bridge import CvBridge

import tf2_ros

import glob
import os

from mask_rcnn_ros.srv import DetectObjects

detect_service = None
cv_bridge = None

def colorize(xyz, zmin = 0, zmax = 3):
    zrng = zmax - zmin
    tmpz = xyz[:,:,2].copy()
    tmpz = ((tmpz - zmin) / zrng * 255)
    tmpz = np.clip(tmpz, 0, 255).astype(np.uint8)

    return cv2.applyColorMap(tmpz, cv2.COLORMAP_JET)

def pc2rgbxyz(points_msg):
    pc = rnp.numpify(points_msg)
    w = points_msg.width
    h = points_msg.height
    xyz = pc.view('<f4').reshape(h,w,pc.itemsize // 4)[:,:,0:3]
    rgb = pc['rgb'].copy().view('<u1').reshape(h,w,4)[:,:,0:3]

    return rgb, xyz

############################################################
# Przetwarzanie chmury punktów
############################################################

def process(score, class_id, class_name, mask, xyz):
    points = xyz[mask!=0]
    c = np.nanmean(points, axis=0)
    rospy.loginfo("Detected %s at %f, %f, %f", class_name, c[0], c[1], c[2])

def callback(points_msg):
    global detect_service

    while True:
        try:
            trans = tfBuffer.lookup_transform(target_frame, points_msg.header.frame_id, points_msg.header.stamp)
            break
        except:
            print("Retry tf lookup")
            rospy.sleep(0.1)
    
    # zamiana chmury punktów na obraz kolorowy oraz macierz współrzędnych
    rgb, xyz = pc2rgbxyz(points_msg)
    
    try:
        image_msg = cv_bridge.cv2_to_imgmsg(rgb, 'bgr8')
        image_msg.header = points_msg.header
        resp = detect_service(image_msg)
        r = resp.result
        rospy.loginfo("Detected %d objects", len(r.scores))

        # process detections
        for sc, ci, cn, ms in zip(r.scores, r.class_ids, r.class_names, r.masks):
            mask = cv_bridge.imgmsg_to_cv2(ms, 'mono8')
            process(sc, ci, cn, mask, xyz)
    except rospy.ServiceException as e:
        print("Service call failed: %s"%e)

    return


if __name__ == '__main__':
    rospy.init_node('symbol_detector', anonymous=True)

    target_frame = rospy.get_param('~target_frame', 'camera_rgb_optical_frame')

    # przygotowanie i wstępne wypełnienie bufora transformacji
    tfBuffer = tf2_ros.Buffer(rospy.Duration(120))
    listener = tf2_ros.TransformListener(tfBuffer)

    cv_bridge = CvBridge()

    rospy.loginfo("Filling TF buffer")
    rospy.sleep(2)


    rospy.loginfo("Subscribing to topics")

    points_sub = rospy.Subscriber('points', PointCloud2, callback, queue_size=1)

    rospy.wait_for_service('detect_objects')
    detect_service = rospy.ServiceProxy('detect_objects', DetectObjects)

    rospy.spin()